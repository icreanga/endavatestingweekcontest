
(function($) {
	var answersEndPointURL = "tutor/answers";
	var answersContainerId = "answers";
	var answersUpdateInterval = 1000; //ms
	
	//when dom is ready
	$(init);
	
	function init() {
		newGetAnswersCycle();
		newQuestionHandler();
	}
	
	function getAnswers() {
		$.get(answersEndPointURL, showAnswers);
	}
	
	function showAnswers(data) {
		$("#" + answersContainerId).html(data);
		
		newGetAnswersCycle();
	}
	
	function newGetAnswersCycle() {
		window.setTimeout(getAnswers, answersUpdateInterval);
	}
	
	function newQuestionHandler() {
		$("#newQuestionForm").on("submit", function() {
			$.post($("#newQuestionForm").attr('action'));
			return false;
		});
	}
})(jQuery);