
(function($) {
	var answerFormId = "saveAnswerForm";
	var saveAnswerForm = null;
	var resultContainerId = "saveResult";
	
	//when dom is ready
	$(init);
	
	function init() {
		saveAnswerForm = $("#" + answerFormId);
		addOnSubmitAnserHandler();
		$('#teamNameForm input').focus();
		$('#teamNameForm').on('submit', onSaveTeamHandler);
	}
	
	function onSaveTeamHandler() {
		$('#nameHidden').val($('#nameInput').val());
		$('#teamNameForm').hide();
		$('#teamName').html($('#nameInput').val());
		
		//show answer form
		$('#' + answerFormId).fadeIn();
		$('#answer').focus();
		
		return false;
	}
	
	function addOnSubmitAnserHandler() {
		saveAnswerForm.on("submit", function() {
										sendAnswer();
										$('#answer').focus().val("");
										return false;
									});
	}
	
	function sendAnswer() {
		var answer = saveAnswerForm.serialize();
		
		$.post(saveAnswerForm.attr("action"), answer, onAnswerSent);
		
		return false;
	}
	
	function onAnswerSent(data) {
		$("#" + resultContainerId).html(data).fadeIn("fast");
		
		setTimeout(function() {
			$("#" + resultContainerId).fadeOut();
		}, 1000);
	}
})(jQuery);