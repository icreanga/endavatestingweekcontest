<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Testing Week Tutor Page</title>
<link href="resources/css/bootstrap.css" rel="stylesheet">
<style type="text/css">
</style>
</head>
<body>
    <div class="container">
		<form class="well form-horizontal span8" action="tutor/newQuestion" method="post" id="newQuestionForm" >
			<div class="form-actions">
	                <button type="submit" class="btn btn-primary">New Question</button>                               
	        </div>
	        
	        <div class="control-group">
		     	<p>Answers to current question:</p>		
				<div id="answers">
				</div>
			</div>
			   
		</form>
	</div>
<script src="resources/js/jquery-2.0.0.min.js" ></script>
<script src="resources/js/tutor-app.js" ></script>
</body>
</html>