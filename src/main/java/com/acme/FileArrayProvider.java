package com.acme;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class FileArrayProvider {
	
	//private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	

    public String[] readLines(String filename, String tillLineFound) throws IOException {
            
        String fullText = readFile(filename);
        int lastQuestionStartIndex = 0;
        
        lastQuestionStartIndex = fullText.lastIndexOf(tillLineFound);      
        
        return fullText.substring(lastQuestionStartIndex + tillLineFound.length()).split("\n");
    }
    
    //http://stackoverflow.com/questions/326390/how-to-create-a-java-string-from-the-contents-of-a-file
    private String readFile( String file ) throws IOException {
        BufferedReader reader = new BufferedReader( new FileReader (file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }
        
        reader.close();

        return stringBuilder.toString();
    }    
    
}