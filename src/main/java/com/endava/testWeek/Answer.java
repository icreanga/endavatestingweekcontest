package com.endava.testWeek;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Answer {
	private String name;
	private String answer;
	private Date timestamp; 
	
	public Answer(String name, String answer,Date date) {
		this.name = name;
		this.answer = answer;
		this.timestamp = date;
	}
	
	public String toString() {
		SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
		return formatter.format(timestamp)+ " "+ name + ":" + answer;
	}
}
