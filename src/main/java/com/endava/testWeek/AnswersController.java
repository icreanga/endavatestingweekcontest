package com.endava.testWeek;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class AnswersController {
	
	@RequestMapping(value = "/tutor/answers", method = RequestMethod.GET)
	public String answers(Locale locale, Model model) {
		
        String[] answers = AnswerSaver.getAnswers();
		
		model.addAttribute("answersa", answers);
		
		return "answers";
	}
}