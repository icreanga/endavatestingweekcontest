package com.endava.testWeek;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Handles requests for the application home page.
 */
@Controller
public class SaveAnswerController {
	
	@RequestMapping(value = "/saveAnswer", method = RequestMethod.POST)
	public String handlePOST(Locale locale, Model model, @RequestParam("name") String name, @RequestParam("answer") String answer) {
				
		try {
			this.saveAnswer(name, answer);
			model.addAttribute("saveResult", "Your answer was succcesfully submitted.");
		} catch(IOException e) {
			model.addAttribute("saveResult", "Your answer could not be saved.");
		}	
		
		return "saveAnswer";
	}
	
	
	private void saveAnswer(String name, String answer) throws IOException {
		Calendar calendar = Calendar.getInstance();		
		AnswerSaver.saveAnswer(new Answer(name, answer,calendar.getTime()));
	}
}