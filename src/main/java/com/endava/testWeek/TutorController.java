package com.endava.testWeek;

import java.io.IOException;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class TutorController {

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/tutor", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		// the name of the jsp file
		return "tutor";
	}

	@RequestMapping(value = "/tutor/newQuestion", method = RequestMethod.POST)
	public String submitNewQuestion(Locale locale, Model model) {
		try {
			AnswerSaver.appendToFile(AnswerSaver.questionSeparator);// new
																	// question
																	// separator
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "newQuestion";
	}
}