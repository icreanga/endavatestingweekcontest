package com.endava.testWeek;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.acme.FileArrayProvider;

public class AnswerSaver {
	static final String fileName = "answers.txt";
	static final String questionSeparator = "###################";
	
	static public String[] getAnswers() {
		FileArrayProvider fap = new FileArrayProvider();
        String[] answers = null;
        
		try {
			answers = fap.readLines(fileName, questionSeparator);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return answers;
	}
	
	static public void saveAnswer(Answer answer) throws IOException {
		appendToFile(answer.toString());
	}
	
	static public void appendToFile (String stringToAppend) throws IOException 
	{
		File file = new File(fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		
		FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.append(stringToAppend);
		bw.newLine();
		bw.close();
	}
}
