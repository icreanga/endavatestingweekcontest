<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Testing Week Contest</title>

<link href="resources/css/bootstrap.css" rel="stylesheet">
<style type="text/css">
#saveAnswerForm {
	display: none;
}

.container form input {
	font-size: 14px;
	height: 25px;
	line-height: 20px;
	display: block;
	
}

 .form.actions,.control-group { margin-left: 100px}

#saveResult {
	display: none;
}

</style>
</head>
<body>
	<div class="container">
	<img src="resources/img/header.jpg" align="middle" style="margin-left:45px;"/>
	</div>
	<div class="container">

		<form class="well form-horizontal span8" method="post" action=""
			id="teamNameForm">
			<div class="control-group" style="margin-top: 25px">
				<label for="name" class="control-label">Choose team name:</label>
				<div class="controls">
					<input type="text" name="name" id="nameInput" class="span3 focused"
						placeholder="your team name" />
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn btn-primary" style="margin-left: 100px">Save Team
					Name</button>
			</div>
		</form>

<div class="container">
		<form class="well form-horizontal span8" method="post"
			action="saveAnswer" id="saveAnswerForm">
			<p style= "margin-left: 163px; margin-top: 25px ; margin-bottom:15px">
				Your team name is set to <b id="teamName"></b>.
			</p>
			<input type="hidden" name="name" id="nameHidden" />
			<div class="control-group">
				<label for="answer" class="control-label">Your answer:</label>
				<div class="controls">
					<input type="text" name="answer" id="answer" class="span3 focused"
						placeholder="your answer" />
				</div>
			</div>

			<div class="form-actions">
				<button type="submit" class="btn btn-primary" style="margin-left: 100px">Submit Answer</button>
			</div>

			<p class="alert alert-info" id="saveResult"></p>
		</form>
		</div>

	</div>

	<script src="resources/js/jquery-2.0.0.min.js"></script>
	<script src="resources/js/app.js"></script>
</body>
</html>
